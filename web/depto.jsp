<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="./partials/head.jsp"/>
<h2 class="d-flex justify-content-center">${titulo}</h2>
<hr>
<div class="d-flex justify-content-around">
    <div class="card col-md-4">
        <div class="card-body">
            <form action="ControllerKv?menu=depto" method="POST">
                <div class="form-group">
                    <label>Codigo:</label>
                    <input type="hidden" name="txtCodigo" value="${depto.getIdDepto()}" class="form-control">
                    <input type="text" name="Codigo" value="${depto.getIdDepto()}" class="form-control" disabled>
                </div>
                <div class="form-group">
                    <label>Departamento:</label>
                    <input type="text" name="txtNombre" value="${depto.getNombre()}" class="form-control">
                </div>
                <div class="form-group">
                    <label>Descripcion:</label>
                    <input type="text" name="txtDescripcion" value="${depto.getDescripcion()}" class="form-control">
                </div>
                <hr>
                <button type="submit" name="accion" value="ingresar" class="btn btn-success">AGREGAR</button>
                <button type="submit" name="accion" value="actualizar" class="btn btn-info">ACTUALIZAR</button>
            </form>
        </div>
    </div>
    <div class="card-md-8">
        <div class="card-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Funciones</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="lstDepto" items="${lstDepto}">
                        <tr>
                            <td>${lstDepto.getIdDepto()}</td>
                            <td>${lstDepto.getNombre()}</td>
                            <td>${lstDepto.getDescripcion()}</td>
                            <td>
                                <a class="btn btn-warning" href="ControllerKv?menu=depto&accion=modificar&idDepto=${lstDepto.getIdDepto()}">Editar</a>
                                <a class="btn btn-danger" href="ControllerKv?menu=depto&accion=eliminar&idDepto=${lstDepto.getIdDepto()}">Eliminar</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>    
        </div>    
    </div>
</div>
<jsp:include page="./partials/footer.jsp"/>