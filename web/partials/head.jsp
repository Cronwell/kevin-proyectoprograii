<%-- 
    Document   : index
    Created on : 11/10/2022, 09:43:23 PM
    Author     : Cronwell
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap demo</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="ControllerKv?menu=home">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                RRHH
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="ControllerKv?menu=depto&accion=consultar">Departamentos</a></li>
                                <li><a class="dropdown-item" href="#">Roles</a></li>
                                <li><a class="dropdown-item" href="#">Permisos</a></li>
                                <li><a class="dropdown-item" href="#">Empleados</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ControllerKv?menu=inventario">INVENTARIO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ControllerKv?menu=ventas">VENTAS</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">