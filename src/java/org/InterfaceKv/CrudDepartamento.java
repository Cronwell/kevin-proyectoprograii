/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.InterfaceKv;

import java.util.List;
import org.ModelKv.ModelDepartamento;

/**
 *
 * @author Cronwell
 */
public interface CrudDepartamento {
    public List listar();
    public ModelDepartamento list(int id);
    public boolean insertar(ModelDepartamento departamento);
    public boolean modificar(ModelDepartamento departamento);
    public boolean eliminar(ModelDepartamento departamento);
}
