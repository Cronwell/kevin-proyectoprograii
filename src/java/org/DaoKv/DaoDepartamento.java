/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.DaoKv;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ConfigKv.Conexion;
import org.InterfaceKv.CrudDepartamento;
import org.ModelKv.ModelDepartamento;

/**
 *
 * @author Cronwell
 */
public class DaoDepartamento implements CrudDepartamento {

    ModelDepartamento depto = new ModelDepartamento();
    String strSql = "";

    Conexion conexion = new Conexion();
    ResultSet rs = null;
    boolean respuesta = false;

    @Override
    public List listar() {
        ArrayList<ModelDepartamento> lstDepartamento = new ArrayList<>();
        try {
            strSql = "select * from departamento";
            conexion.open();
            rs = conexion.executeQuery(strSql);

            while (rs.next()) {
                ModelDepartamento depa = new ModelDepartamento();
                depa.setIdDepto(rs.getInt("idDepto"));
                depa.setNombre(rs.getString("nombre"));
                depa.setDescripcion(rs.getString("descripcion"));

                lstDepartamento.add(depa);
            }

            rs.close();
            conexion.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lstDepartamento;
    }

    @Override
    public ModelDepartamento list(int id) {
        try {
            strSql = "select * from departamento where idDepto = " + id;
            conexion.open();
            rs = conexion.executeQuery(strSql);

            while (rs.next()) {
                depto.setIdDepto(rs.getInt("idDepto"));
                depto.setNombre(rs.getString("nombre"));
                depto.setDescripcion(rs.getString("descripcion"));
            }

            rs.close();
            conexion.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return depto;
    }

    @Override
    public boolean insertar(ModelDepartamento departamento) {
        strSql = "insert into departamento (nombre, descripcion) values ("
                + "'" + departamento.getNombre() + "',"
                + "'" + departamento.getDescripcion() + "');";
        System.out.println(strSql);
        try {
            conexion.open();
            respuesta = conexion.executeSql(strSql);
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, e);
        }

        return respuesta;
    }

    @Override
    public boolean modificar(ModelDepartamento departamento) {
        strSql = "update departamento "
                + "set "
                + "nombre = '" + departamento.getNombre() + "', "
                + "descripcion = '"+ departamento.getDescripcion() + "' "
                + "where idDepto = "+ departamento.getIdDepto();
        
        try {
            conexion.open();
            respuesta = conexion.executeSql(strSql);
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

    @Override
    public boolean eliminar(ModelDepartamento departamento) {
        strSql = "delete from departamento where idDepto = " + departamento.getIdDepto();
        
        try {
            conexion.open();
            respuesta = conexion.executeSql(strSql);
            conexion.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            Logger.getLogger(DaoDepartamento.class.getName()).log(Level.SEVERE, null, e);
        }
        return respuesta;
    }

}
