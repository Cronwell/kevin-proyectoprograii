/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ConfigKv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Cronwell
 */
public class Conexion {
    private PreparedStatement preparar = null;
    private Connection coneccion = null;
    private ResultSet resultado = null;
    private boolean respuesta = false;
    
    //Cadena de conexion para mysql
    //String stringConnectionUrl = "jdbc:mysql://localhost:3306/kvproyectoprogra2";
     
    //Driver o controlador JDBC
    //String driver = "com.mysql.jdbc.Driver";
    
    public Connection open() throws ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            coneccion = DriverManager.getConnection("jdbc:mysql://localhost:3306/kvproyectoprogra2", "root", "");
        } catch (SQLException e) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Error: "+e.getMessage());
        }
        return coneccion;
    }
    
    public void close() throws Exception{
        try {
            if(coneccion != null){
                coneccion.clearWarnings();
                coneccion.close();
            }
        } catch (SQLException e) {
            coneccion = null;
            throw new Exception(e.getMessage());
        }
    }
    
    //INSERT, UPDATE, DELETE
    public boolean executeSql(String cmd) throws Exception{
        if (cmd != null)
            try {
                    this.preparar = this.coneccion.prepareStatement(cmd);
                    this.preparar.executeUpdate();
                    respuesta = true;
            } catch (SQLException e) {
                    throw new Exception(e.getMessage());
            }
            else
                    throw new Exception("El comando a ejecutar es nulo!");
            return respuesta;
    }
    
    //SELECT
    public ResultSet executeQuery(String strSQL){
        if(strSQL != null){
            try {
                preparar = coneccion.prepareStatement(strSQL);
                resultado = preparar.executeQuery();
            } catch (SQLException e) {
                System.out.println("Error al ejecutar el query en la Clase: Conexion: "+e.toString());
            }
        }
        //close();
        return resultado;
    }
}
