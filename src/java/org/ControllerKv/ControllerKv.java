/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ControllerKv;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.DaoKv.DaoDepartamento;
import org.ModelKv.ModelDepartamento;

/**
 *
 * @author Cronwell
 */
public class ControllerKv extends HttpServlet {

    ModelDepartamento depto = new ModelDepartamento();
    DaoDepartamento daoDepto = new DaoDepartamento();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String menu = request.getParameter("menu").toLowerCase();
        String titulo = "";
        String accion = "";

        switch (menu) {
            case "rrhh":
                titulo = "Quien lo diria estoy en rrhh";
                request.setAttribute("titulo1", titulo);
                request.getRequestDispatcher("./rrhh.jsp").forward(request, response);
                break;
            case "depto":
                String nombre = "";
                String desc = "";
                int idDepto = 0;
                List lstDepto = daoDepto.listar();
                titulo = "Departamentos";
                accion = request.getParameter("accion").toLowerCase();
                switch (accion) {
                    case "consultar":
                        lstDepto = daoDepto.listar();
                        request.setAttribute("lstDepto", lstDepto);
                        request.setAttribute("titulo", titulo);
                        request.getRequestDispatcher("./depto.jsp").forward(request, response);
                        break;
                    case "ingresar":
                        nombre = request.getParameter("txtNombre");
                        desc = request.getParameter("txtDescripcion");

                        depto.setNombre(nombre);
                        depto.setDescripcion(desc);

                        daoDepto.insertar(depto);
                        
                        request.getRequestDispatcher("ControllerKv?menu=depto&accion=consultar").forward(request, response);
                        break;
                    case "modificar":
                        idDepto = Integer.parseInt(request.getParameter("idDepto"));
                        ModelDepartamento deptoMod = new ModelDepartamento();
                        deptoMod = daoDepto.list(idDepto);
                        
                        request.setAttribute("depto", deptoMod);
                        request.getRequestDispatcher("ControllerKv?menu=depto&accion=consultar").forward(request, response);
                        break;
                    case "actualizar":
                        idDepto = Integer.parseInt(request.getParameter("txtCodigo"));
                        nombre = request.getParameter("txtNombre");
                        desc = request.getParameter("txtDescripcion");
                        
                        depto.setNombre(nombre);
                        depto.setDescripcion(desc);
                        depto.setIdDepto(idDepto);
                        
                        daoDepto.modificar(depto);
                        
                        request.getRequestDispatcher("ControllerKv?menu=depto&accion=consultar").forward(request, response);
                        break;
                    case "eliminar":
                        idDepto = Integer.parseInt(request.getParameter("idDepto"));
                        
                        depto.setIdDepto(idDepto);
                        daoDepto.eliminar(depto);
                                                
                        request.getRequestDispatcher("ControllerKv?menu=depto&accion=consultar").forward(request, response);
                        break;
                    default:
                        request.setAttribute("lstDepto", lstDepto);
                        request.setAttribute("titulo", titulo);
                        request.getRequestDispatcher("./depto.jsp").forward(request, response);
                }
            default:
                request.getRequestDispatcher("./index.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
