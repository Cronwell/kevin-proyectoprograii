-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema KvProyectoProgra2
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema KvProyectoProgra2
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `KvProyectoProgra2` DEFAULT CHARACTER SET utf8 ;
USE `KvProyectoProgra2` ;

-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`departamento` (
  `idDepto` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idDepto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`rol` (
  `idRol` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  `estado` INT NULL,
  PRIMARY KEY (`idRol`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`empleado` (
  `idEmpleado` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `puesto` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NULL,
  `idRol` INT NOT NULL,
  `idDepto` INT NULL,
  PRIMARY KEY (`idEmpleado`),
  INDEX `usuario_depto_idx` (`idDepto` ASC) VISIBLE,
  INDEX `usuario_rol_idx` (`idRol` ASC) VISIBLE,
  CONSTRAINT `usuario_depto`
    FOREIGN KEY (`idDepto`)
    REFERENCES `KvProyectoProgra2`.`departamento` (`idDepto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `usuario_rol`
    FOREIGN KEY (`idRol`)
    REFERENCES `KvProyectoProgra2`.`rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`permiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`permiso` (
  `idPermiso` INT NOT NULL AUTO_INCREMENT,
  `idRol` INT NULL,
  `estado` INT NULL,
  PRIMARY KEY (`idPermiso`),
  INDEX `permiso_rol_idx` (`idRol` ASC) VISIBLE,
  CONSTRAINT `permiso_rol`
    FOREIGN KEY (`idRol`)
    REFERENCES `KvProyectoProgra2`.`rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`marca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`marca` (
  `idMarca` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`idMarca`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`proveedor` (
  `idProveedor` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `telefono` INT NULL,
  `email` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  PRIMARY KEY (`idProveedor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `idMarca` INT NULL,
  `modelos` INT NULL,
  `descripcion` VARCHAR(45) NULL,
  `idProveedor` INT NULL,
  `stock` DOUBLE NULL,
  `precioA` DOUBLE NULL,
  `precioB` DOUBLE NULL,
  `precioC` DOUBLE NULL,
  PRIMARY KEY (`idProducto`),
  INDEX `producto_marca_idx` (`idMarca` ASC) VISIBLE,
  INDEX `producto_proveedor_idx` (`idProveedor` ASC) VISIBLE,
  CONSTRAINT `producto_marca`
    FOREIGN KEY (`idMarca`)
    REFERENCES `KvProyectoProgra2`.`marca` (`idMarca`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `producto_proveedor`
    FOREIGN KEY (`idProveedor`)
    REFERENCES `KvProyectoProgra2`.`proveedor` (`idProveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`cliente` (
  `idCliente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `telefono` INT NULL,
  `nit` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`encFac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`encFac` (
  `idEncFac` INT NOT NULL AUTO_INCREMENT,
  `numeroFac` INT NULL,
  `serieFac` VARCHAR(45) NULL,
  `fecha` DATE NULL,
  `total` DOUBLE NULL,
  `idCliente` INT NULL,
  PRIMARY KEY (`idEncFac`),
  INDEX `encFac_cliente_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `encFac_cliente`
    FOREIGN KEY (`idCliente`)
    REFERENCES `KvProyectoProgra2`.`cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `KvProyectoProgra2`.`detFac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KvProyectoProgra2`.`detFac` (
  `idDetFac` INT NOT NULL AUTO_INCREMENT,
  `idProducto` INT NULL,
  `idEmpleado` INT NULL,
  `cantidad` DOUBLE NULL,
  `subTotal` DOUBLE NULL,
  `idEncFac` INT NULL,
  PRIMARY KEY (`idDetFac`),
  INDEX `detFac_idx` (`idEncFac` ASC) VISIBLE,
  INDEX `detFac_empleado_idx` (`idEmpleado` ASC) VISIBLE,
  INDEX `detFac_producto_idx` (`idProducto` ASC) VISIBLE,
  CONSTRAINT `detFac_encFac`
    FOREIGN KEY (`idEncFac`)
    REFERENCES `KvProyectoProgra2`.`encFac` (`idEncFac`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `detFac_empleado`
    FOREIGN KEY (`idEmpleado`)
    REFERENCES `KvProyectoProgra2`.`empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `detFac_producto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `KvProyectoProgra2`.`producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
